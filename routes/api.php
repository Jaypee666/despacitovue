<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/student/create', ['as' => 'create-student', 'uses' => 'StudentsController@createStudent']);
Route::get('/students', ['as' => 'get-students', 'uses' => 'StudentsController@readStudents']);
Route::post('/student/update', ['as' => 'update-student', 'uses' => 'StudentsController@updateStudent']);
Route::delete('/student/delete/{id}', ['as' => 'delete-student', 'uses' => 'StudentsController@deleteStudent']);