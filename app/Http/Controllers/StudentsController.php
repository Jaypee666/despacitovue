<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentsController extends Controller
{
    public function createStudent(Request $request)
    {
    	$validatedData = $request->validate([
	        'first_name' => 'required',
	        'last_name' => 'required',
	        'middle_name' => 'required',
	    ]);

	    if(Student::createStudent($request) == 500) {
	    	return "Fail";
	    } else {
	    	$students = Student::readStudents();
    		echo $students;
	    }
    }

    public function readStudents()
    {
    	$students = Student::readStudents();
    	echo $students;
    }

    public function updateStudent(Request $request)
    {
    	$validatedData = $request->validate([
	        'first_name' => 'required',
	        'last_name' => 'required',
	        'middle_name' => 'required',
	    ]);

    	if(Student::updateStudent($request) == 500) {
	    	return "Fail";
	    } else {
	    	$students = Student::readStudents();
    		echo $students;
	    }
    }

    public function deleteStudent($id)
    {
    	Student::deleteStudent($id);
    	$students = Student::readStudents();
    	echo $students;
    }
}
