<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
	use SoftDeletes;

    protected $table = 'students';
    

    public static function createStudent($request) 
    {

    	$student = new Student();
    	$student->first_name = $request->first_name;
    	$student->last_name = $request->last_name;
    	$student->middle_name = $request->middle_name;
    	if($student->save()) {
    		return 100;
    	} else {
    		return 500;
    	}
    }

    public static function readStudents() 
    {
    	$students = Student::orderBy('created_at', 'desc')->paginate(5);
    	$students = json_encode($students);
    	return $students;
    }

    public static function updateStudent($request) 
    {
    	$student = Student::find($request->id);
    	$student->first_name = $request->first_name;
    	$student->last_name = $request->last_name;
    	$student->middle_name = $request->middle_name;
    	$student->save();
    }

    public static function deleteStudent($id) 
    {
    	$student = Student::find($id);
    	$student->delete();
    }
}
